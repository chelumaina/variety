<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('variety_model');
    }

    public function index()
    { 
        $data['region'] = $this->variety_model->variety_region();
        $this->load->view('header');
        $this->load->view('dashboard', $data);
        $this->load->view('footer');
    }
    public function about()
    { 
        $data['region'] = $this->variety_model->variety_region();
        $this->load->view('header');
        $this->load->view('about', $data);
        $this->load->view('footer');
    }
    public function map()
    { 
        $data['region'] = $this->variety_model->variety_region();
        $this->load->view('header');
        $this->load->view('map', $data);
        $this->load->view('footer');
    }
    
    

    public function search_variety()
    {

        //$data['varieties'] = $this->variety_model->variety();
        $data['region'] = $this->variety_model->variety_region();
        $this->load->view('header');
        $this->load->view('search', $data);
        $this->load->view('footer');
    }

    public function variety()
    {

        $data['varieties'] = $this->variety_model->variety();
        $this->load->view('header');
        $this->load->view('variety', $data);
        $this->load->view('footer');
    }

    function get_variety_region()
    {
        $variety = $this->input->post('variety');
        $res = $this->variety_model->variety_in_region($variety);
        $s = '<option value=""> Select Province</option>';
        foreach ($res as $r)
        {
            $s .= '<option value="' . $r->Region . '">' . $r->Region . '</option>';
        }
        echo $s;
    }

    function get_station()
    {
        $region = $this->input->post('region');
        $res = $this->variety_model->variety_station($region);
        $s = '<option value=""> Select District</option>';
        foreach ($res as $r)
        {
            $s .= '<option value="' . $r->Station . '">' . $r->Station . '</option>';
        }
        echo $s;
    }

    function get_month_day()
    {
        $region = $this->input->post('region');
        $station = $this->input->post('station');
        $res = $this->variety_model->variety_month_day($region, $station);
        $s = '<option value=""> Select Month Day</option>';
        foreach ($res as $r)
        {
            $s .= '<option value="' . $r->Month_Day . '">' . $r->Month_Day . '</option>';
        }
        echo $s;
    }

    function search()
    {
        $region = $this->input->post('region');
        $station = $this->input->post('station');
        $month_day = $this->input->post('month_day');
        $res = $this->variety_model->variety_result($region, $station, $month_day);
        $s = '<table class="table table-stripped table-bordered">';


        $y = '';
        $s .= '<tr><th>Region</th><th>Station</th><th>Month_Day</th><th>sum</th><th>mean</th><th>sd</th><th>var</th><th>Latitude</th><th>Longitude</th></tr>';
        foreach ($res as $r)
        {
            $y .= 'You have Selected variety ' . $r->Cultivar . ' in ' . $r->Station . '(' . $r->Latitude . ', ' . $r->Longitude . ' Under Province ' . $r->Region . ' and the days take for this variety to mature is' . $r->mean . ' with a variation
of ' . $r->var . ' ';
            $s .= '<tr><td>' . $r->Region . '</td><td>' . $r->Station . '</td><td>' . $r->Month_Day . '</td><td>' . $r->sum . '</td><td>' . $r->mean . '</td><td>' . $r->sd . '</td><td>' . $r->var . '</td><td>' . $r->Latitude . '</td><td>' . $r->Longitude . '</td></tr>';
        }
        $s .= '</table> ';
        echo $y;
    }

    function search_cultivar()
    {
        $variety = $this->input->post('variety');
        $region = $this->input->post('region');
        $station = $this->input->post('station');
        $month_day = $this->input->post('month_day');
        $res = $this->variety_model->variety_result2($variety, $region, $station, $month_day);


        $s = '<table class="table table-stripped table-bordered"><tbody>';


        $y = '';
        $s .= '<tr><th></th><th>Output</th>  </tr>';
        $i = 1;
        foreach ($res as $r)
        {
            //$havest_date = new DateTime($maturity);
            //$date_h = $havest_date->format('Y-m-d');

            $date = explode('_', $r->Month_Day);

            $mnth = str_pad($date[0], 2, "0", STR_PAD_LEFT);
            $day = str_pad($date[1], 2, "0", STR_PAD_LEFT);

            $d = new DateTime(date('Y') . '-' . $mnth . '-' . $day);


            $da2 = $d->format('Y-m-d');

            $d->modify('+' . round($r->mean) . ' day');
            $da = $d->format('Y-m-d');


            $x = '<table class="table table-stripped table-bordered"><tbody>'
                    . '<tr><th width="50%">Variety name: (variety list)</th><td width="50%">' . $r->Cultivar . '</td></tr>'
                    . '<tr><th>Province </th><td>' . $r->Station . ' (' . $r->Latitude . ',' . $r->Longitude . ')</td></tr>'
                    . '<tr><th>District:</th><td>' . $r->Region . '</td></tr>'
                    . '<tr><th>Days to Mature</th><td>' . $r->mean . '</td></tr>'
                    . '<tr><th>Date:</th><td>' . $da2 . '  - ' . $da . '</td></tr>'
                    . '<tr><th>Maturity day may vary with (+/-days)</th><td>' . $r->sd . '</td></tr>'
                    . '</tbody></table> ';
            $s .= '<tr><td>' . $i . '</td><td>' . $x . '</td></tr>';
            $i++;
        }
        $s .= '</tbody></table> ';
        echo $s;
    }

    function search_maturity()
    {

        $region = $this->input->post('region');
        $station = $this->input->post('station');
        $maturity = $this->input->post('maturity');
        $planting = $this->input->post('planting');

        $earlier = new DateTime($planting);
        $later = new DateTime($maturity);

        $diff = $later->diff($earlier)->format("%a");
         
        $pl_month= $earlier->format('n') ;
        $pl_day= $earlier->format('j') ;
        
        $mt_month= $later->format('n') ;
        $mt_day= $later->format('j') ; 

        $res = $this->variety_model->variety_result3($region, $station, $pl_month, $pl_day, $mt_month, $mt_day, $diff);
        
        
        $s = '<table class="table table-stripped table-bordered"><tbody>';


        $y = '';
        $s .= '<tr><th></th><th>Output</th>  </tr>';
        $i = 1;
        foreach ($res as $r)
        {
            $havest_date = new DateTime($maturity);
            $date_h = $havest_date->format('Y-m-d');

            $date = explode('_', $r->Month_Day);

            $mnth = str_pad($date[0], 2, "0", STR_PAD_LEFT);
            $day = str_pad($date[1], 2, "0", STR_PAD_LEFT);
            $start_date = date('Y') . '-' . $mnth . '-' . $day . ' 00:00:00';



            $d = new DateTime(date('Y') . '-' . $mnth . '-' . $day);

            $d_planting = new DateTime(date('Y') . '-' . $mnth . '-' . $day);

            $d_planting->modify('+' . round($r->mean) . ' day');
            $da = $d_planting->format('Y-m-d');


            $stop_date = date('Y-m-d h:m:s', strtotime($start_date . ' +' . $r->mean . ' day'));
            
              $x = '<table class="table table-stripped table-bordered"><tbody>'
                    . '<tr><th width="50%">Variety name: (variety list)</th><td width="50%">' . $r->Cultivar . '</td></tr>'
                    . '<tr><th>Province </th><td>' . $r->Station . ' (' . $r->Latitude . ',' . $r->Longitude . ')</td></tr>'
                    . '<tr><th>District:</th><td>' . $r->Region . '</td></tr>'
                    . '<tr><th>Days to Mature</th><td>' . $r->mean . '</td></tr>'
                    . '<tr><th>Date:</th><td>' .  $da . '</td></tr>'
                    . '<tr><th>Maturity day may vary with (+/-days)</th><td>' . $r->sd . '</td></tr>'
                    . '<tr><th>Preferred Planting date: (incase the user has selected the module where they key in there preferred maturity day to get there planting dates )</th><td>' . $planting. '</td></tr>'
                    . '</tbody></table> ';
            $s .= '<tr><td>' . $i . '</td><td>' . $x . '</td></tr>';
            
 
            $i++;
        }
        $s .= '</tbody></table> ';
        echo $s;
    }

}
