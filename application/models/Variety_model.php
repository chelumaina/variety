<?php

class Variety_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function variety_region()
    {
        $this->db->select('Region');
        $this->db->from('varieties');
        $this->db->group_by('Region');
        return $this->db->get()->result();
    }

    function variety()
    {
        $this->db->select('Cultivar');
        $this->db->from('varieties');
        $this->db->group_by('Cultivar');
        return $this->db->get()->result();
    }

    function variety_station($region)
    {
        $this->db->select('Station');
        $this->db->from('varieties');
        $this->db->where('Region', $region);
        $this->db->group_by('Station');
        return $this->db->get()->result();
    }

    function variety_in_region($Cultivar)
    {
        $this->db->select('Region');
        $this->db->from('varieties');
        $this->db->where('Cultivar', $Cultivar);
        $this->db->group_by('Region');
        return $this->db->get()->result();
    }

    function variety_month_day($region, $station)
    {
        $this->db->select('Month_Day');
        $this->db->from('varieties');
        $this->db->where('Region', $region);
        $this->db->where('Station', $station);
        $this->db->group_by('Month_Day');
        return $this->db->get()->result();
    }

    function variety_result($region, $station, $mon_day)
    {
        $this->db->select('*');
        $this->db->from('varieties');
        $this->db->where('Region', $region);
        $this->db->where('Station', $station);
        $this->db->where('Month_Day', $mon_day);
        return $this->db->get()->result();
    }

    function variety_result2($var, $region, $station, $mon_day)
    {
        $this->db->select('*');
        $this->db->from('varieties');
        $this->db->where('Cultivar', $var);
        $this->db->where('Region', $region);
        $this->db->where('Station', $station);
        $this->db->where('Month_Day', $mon_day);
        return $this->db->get()->result();
    }

    function variety_result3($region, $station, $pl_month, $pl_day, $mt_month, $mt_day,  $days)
    {
        $sql="select * from varieties 
                where 
                Region='".$region."'
                AND Station='".$station."'
                AND SUBSTR(Month_Day,1,1)>='".$pl_month."' and SUBSTR(Month_Day,3,1)>='".$pl_day."' 
                AND ".$days." BETWEEN floor(mean)-5 AND round(mean)+5";
      
        return $this->db->query($sql)->result();
    }

}
