<!DOCTYPE html> 
<html dir="ltr" lang="en" class=" js no-touch csstransitions" style="height: auto;"><!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Variety</title>
        <meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
        <meta name="author" content="htmlcoder.me"> 

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="">
        <base href="<?php echo base_url() ?>" />

        <link href="<?php echo base_url() ?>includes/vendors/odesk/globals.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>includes/folder/style.css" rel="stylesheet">



        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url() ?>includes/folder/bootstrap.css" rel="stylesheet">

        <!-- Font Awesome CSS -->
        <link href="<?php echo base_url() ?>includes/folder/font-awesome.css" rel="stylesheet">

        <!-- Fontello CSS -->
        <link href="<?php echo base_url() ?>includes/folder/fontello.css" rel="stylesheet">

        <!-- Plugins -->
        <link href="<?php echo base_url() ?>includes/folder/magnific-popup.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>includes/folder/settings.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>includes/folder/layers.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>includes/folder/navigation.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>includes/folder/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>includes/folder/owl.theme.default.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>includes/folder/hover-min.css" rel="stylesheet">		

        <link href="<?php echo base_url() ?>includes/folder/style.css" rel="stylesheet">

        <link href="<?php echo base_url() ?>includes/folder/custom.css" rel="stylesheet">
        <style>
            .navbar-default {
                background-color: transparent;
                border-color: transparent;
            }
            .errorField2, .errorField{border:1px solid red;}
            html{    background: #cfb53b;}

            .page-wrapper {
                background:  #cfb53b;
            }
            .main {
                min-height: 450px!important;
            }
            .container {
                position: relative;
            }
            .object-non-visible
            {
                background-color: #fff;
                min-height: 450px!important;

            }
            th {
                background: #effeae!important;font-weight: bolder;
            }
            h3{color: #CFB53B;font-weight: bolder;}

            .btn-primary {
                border-color: #22732d;
                color: #fff;
                background-color: #293503!important;
            }

            .btn-primary:hover {
                background-color: #CFB53B!important;
            }

            .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
                background-color: transparent;
                color: #293503;
            }

          

            .oIndicatorMsg {
                position: fixed;
                display: inline-block;
                top: 100px;
                left: 45%;
            }


            .oLoading, .oIndicatorMsg {
                -webkit-box-shadow: 0 3px 3px rgba(250,200,75,.3);
                -moz-box-shadow: 0 3px 3px rgba(250,200,75,.3);
                box-shadow: 0 3px 3px rgba(250,200,75,.3);
                padding: 8px 20px 10px 16px;
                border-radius: 7px;
                border: 2px solid #fad390;
                background: #fff9e5;
                color: #c56b00;
                font-size: 13px;
            }

            .oLoading, .oIndicatorMsg, .oLoadingLrg, .oIndicatorLrg {
                display: table;
                margin: 0 auto;
                font-weight: 700;
                text-align: center;
                z-index: 21000;
                -webkit-animation: pulse .7s ease-in-out 0s 1;
                animation: pulse .7s ease-in-out 0s 1;
            }

            * {
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }

            body { 
                padding-bottom: 0px!important;
            }

            legend {
                display: block;
                padding: 0;
                margin-bottom: 4px!important;
                font-size: 13px!important;
                line-height: inherit;
                color: #333;
                border: 0;
                border-bottom: 1px solid #e5e5e5;
                margin-left: 10px!important;
            }
        </style>
    </head>

    <body class="front-page   page-loader-4  wide pace-done">
        <div id="" class="">
            <div class="placeholder oLoading oIndicatorMsg hide"> Please Wait...</div>
        </div>

 
        <div class="page-wrapper">
 
            <div >

 
                <div class="header-top  ">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-3 col-sm-6 col-md-7">
                                <!-- header-top-first start -->
                                <!-- ================ -->
                                <div class="header-top-first clearfix">

                                    <ul class="list-inline hidden-sm hidden-xs">
                                        <li><i class="fa fa-phone pr-5 pl-10"></i><a style="color:#fff;">+254720000000</a></li>
                                        <li><i class="fa fa-envelope-o pr-5 pl-10"></i> <a style="color:#fff;">example@example.com</a></li>
                                    </ul>
                                </div>
                                <!-- header-top-first end -->
                            </div>
                            <div class="col-xs-9 col-sm-6 col-md-5">

                                <!-- header-top-second start -->
                                <!-- ================ -->
                                <div id="header-top-second" class="clearfix">

                                    <!-- header top dropdowns start -->
                                    <!-- ================ -->
                                    <div class="header-top-dropdown text-right">
 
                                            <div class="btn-group">
                                                <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-user pr-10"></i> Sign Up</a>
                                            </div>

                                            <div class="btn-group">
                                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-lock pr-10"></i> Login</a>
                                            </div>
                                      
                                    </div>
                                    <!--  header top dropdowns end -->
                                </div>
                                <!-- header-top-second end -->
                            </div>
                        </div>
                    </div>
                </div>
                
                <header class="header centered fixed    clearfix">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <!-- header-first start -->
                                <!-- ================ -->
                                <div class="header-first clearfix">

                                    <!-- logo -->
                                    <div id="logo" class="logo">
                                        <a href="">
                                            <img id="logo_img2" src="MVS" alt="MVS">
                                        </a>
                                    </div>



                                </div>
                                <!-- header-first end -->

                            </div>
                            <div class="col-md-8">

                                <!-- header-second start -->
                                <!-- ================ -->
                                <div class="header-second clearfix">

                                    <!-- main-navigation start -->
                                    <!-- classes: -->
                                    <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                                    <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                                    <!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
                                    <!-- ================ -->
                                    <div class="main-navigation  animated ">

                                        <!-- navbar start -->
                                        <!-- ================ -->
                                        <nav class="navbar navbar " role="navigation">
                                            <div class="container-fluid">

                                                <!-- Toggle get grouped for better mobile display -->
                                                <div class="navbar-header">
                                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                                        <span class="sr-only">Toggle navigation</span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                    </button>

                                                </div>

                                                <!-- Collect the nav links, forms, and other content for toggling -->
                                                <div class="collapse navbar-collapse  navbar-right" id="navbar-collapse-1" style="margin-top: 20px;">
                                                    <!-- main-menu -->
                                                    <ul class="nav navbar-nav navbar-right">
                                                        
                                                        <li class="active mega-menu" style="">
                                                            <a href="<?php echo site_url('welcome')?>"> <i class="fa fa-home"></i>  Home</a>
                                                        </li>
                                                        <li class="active mega-menu" style="">
                                                            <a href="<?php echo site_url('welcome/about')?>"> <i class="fa fa-info"></i>  About</a>
                                                        </li>
                                                        
                                                        <li class="active mega-menu" style="">
                                                            <a href="<?php echo site_url('welcome/variety')?>"> <i class="fa fa-map"></i>  Variety</a>
                                                        </li>
                                                        <li class="active mega-menu" style="">
                                                            <a href="<?php echo site_url('welcome/search_variety')?>"><i class="fa fa-crop"></i>  Planting/Maturity</a>
                                                        </li>
                                                        <li class="active mega-menu" style="">
                                                            <a href="<?php echo site_url('welcome/search_variety')?>"><i class="fa fa-search"></i>  Search</a>
                                                        </li>
                                                        <li class="active mega-menu" style="">
                                                            <a href="<?php echo site_url('welcome/map')?>"><i class="fa fa-map"></i>  Map</a>
                                                        </li>
                                               

                                                    </ul>
                                                    <!-- main-menu end -->

                                                </div>

                                            </div>
                                        </nav>
                                        <!-- navbar end -->

                                    </div>
                                    <!-- main-navigation end -->
                                </div>
                                <!-- header-second end -->

                            </div>
                        </div>
                    </div>


                </header>
                <!-- header end -->
            </div>
