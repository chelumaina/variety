
<link href="includes/vendors/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
<!-- main-container start -->
<!-- ================ -->
<section class="main-container main object-non-visible">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->

            
            
                        <div class=" col-md-12"> 
                  <!-- page-title end -->
                <form class="form-horizontal" method="post" id="search" role="search">
                    <div class="row">
                        <div class="col-md-3">
                            <select name="region" id="region">
                                <option value=""> Select Province</option>
                                <?php
                                foreach ($region as $r)
                                {
                                    echo '<option value="' . $r->Region . '">' . $r->Region . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="station" id="station">
                                <option value=""> Select District</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input type="date" class="form-control" name="planting" id="planting" placeholder="Planting Date" />
                        </div>
                        <div class="col-md-2">
                            <input type="date" class="form-control" name="maturity" id="maturity" placeholder="Maturity Date" />
                        </div>

                        <div class="col-md-2">
                            <button name="show" id="show" class="btn btn-primary btn-md">Show Results</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12 result"></div>
                </div>



            </div>
            <!-- main end -->
 
        </div>
    </div>
</section>
<!-- main-container end -->

<script src="<?php echo base_url() ?>includes/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="includes/vendors/select2/dist/js/select2.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function (e) {

          $('#region').select2({
            'theme': 'classic',
            'width': '100%'
        });
        $('#station').select2({
            'theme': 'classic',
            'width': '100%'
        });
      
        $('#variety').select2({
            'theme': 'classic',
            'width': '100%'
        });


        $('#variety').on('change', function () {
            $.ajax({
                context: this,
                type: 'POST',
                url: "<?php echo site_url('welcome/get_variety_region') ?>",
                data: {variety: $('#variety').val()},
                //contentType: 'application/json',
                success: function (data) {

                    $('#region').html(data);

                },
                error: function (jqXHR, exception) {
                    return false;
                }
            });
        });

        $('#region').on('change', function () {
            $.ajax({
                context: this,
                type: 'POST',
                url: "<?php echo site_url('welcome/get_station') ?>",
                data: {region: $('#region').val()},
                //contentType: 'application/json',
                success: function (data) {
//                    var json = JSON.parse(data);
//                    $.each(json, function (index, value) {
//                        if (index === "error")
//                        {
                    $('#station').html(data);
//                        } else
//                        {
//                            window.location.reload();
//                        }
//                    });
//                    //console.log(""+data);
                },
                error: function (jqXHR, exception) {
                    return false;
                }
            });
        });

        $('#station').on('change', function () {
            $.ajax({
                context: this,
                type: 'POST',
                url: "<?php echo site_url('welcome/get_month_day') ?>",
                data: {station: $('#station').val(), region: $('#region').val()},
                //contentType: 'application/json',
                success: function (data) {
//
////                    var json = JSON.parse(data);
////                    $.each(json, function (index, value) {
////                        if (index === "error")
//                        {
                    $('#month_day').html(data);
//                        } else
//                        {
//                            window.location.reload();
//                        }
//                    });
//                    //console.log(""+data);
                },
                error: function (jqXHR, exception) {
                    return false;
                }
            });

        });

        $("#show").click(function (e) {
            e.preventDefault();
            $.ajax({
                context: this,
                type: 'POST',
                url: "<?php echo site_url('welcome/search_maturity') ?>", 
                data: $("#search").serialize(),
                //contentType: 'application/json',
                success: function (data) {

                    $(".result").html(data);

                },
                error: function (jqXHR, exception) {
                    return false;
                }
            });
        });
    })


</script>

