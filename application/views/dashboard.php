<style>
    article{border:2px gray solid;border-radius: 5px;padding: 19px;margin-bottom: 20px;}
    p {
        display: block;
        -webkit-margin-before: 1em;
        -webkit-margin-after: 1em;
        -webkit-margin-start: 0px;
        -webkit-margin-end: 0px;
        font-size: 16px;
    }



    html, body {
        font-family: Verdana,sans-serif;
        font-size: 15px;
        line-height: 1.5;
    }

    h4{font-size: 24px;font-weight: bolder;}
    .main-container {
        padding: 5px 0; 
    }
</style>
<!-- main-container start -->
<!-- ================ -->
<section class="main-container main object-non-visible">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class=" col-md-12"> 
                <h1 style="text-align: center;color:blue;font-weight: bolder;">Maize-Variety-Selector </h1>
                <h4 style="text-align: center;color:black;font-weight: bolder;">Identify the best adapted varieties for your location</h4>
            </div>
        </div>
        <div class="row">
            <div class=" col-md-12">  
                <img width="100%" src="images/Maize.jpg" alt=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"><hr /></div>
        </div>
        <div class="row">
            <div class=" col-md-6">  
                <article>
                    <h4>Predicting time to flowering and maturity</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
                </article>
            </div>
             <div class=" col-md-6">

                <article style="background-image: linear-gradient(white, #dfff80);">
                    <h4>Mapping varietal adaptation</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 

                </article>
            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->

<script src="<?php echo base_url() ?>includes/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="includes/vendors/select2/dist/js/select2.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function (e) {


    })


</script>

