
<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
<!-- ================ -->
<footer id="footer" class="clearfix hide">

    <div class="subfooter" style="background-color: #293503;">
        <div class="container">
            <div class="subfooter-inner">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">Copyright @ 2017 <a target="_blank" href="#">www.judiciary.go.ke</a>. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .subfooter end -->

</footer>
<!-- footer end -->

</div>


<script type="text/javascript" src="<?php echo base_url() ?>includes/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>