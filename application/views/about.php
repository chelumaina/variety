<style>
    article{border:2px gray solid;border-radius: 5px;padding: 19px;margin-bottom: 20px;}
    p {
        display: block;
        -webkit-margin-before: 1em;
        -webkit-margin-after: 1em;
        -webkit-margin-start: 0px;
        -webkit-margin-end: 0px;
        font-size: 16px;
    }



    html, body {
        font-family: Verdana,sans-serif;
        font-size: 15px;
        line-height: 1.5;
    }

    h4{font-size: 24px;font-weight: bolder;}
    .main-container {
        padding: 5px 0; 
    }
</style>
<!-- main-container start -->
<!-- ================ -->
<section class="main-container main object-non-visible">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class=" col-md-12"> 
                <h1 style="text-align: center;color:blue;font-weight: bolder;">Maize-Variety-Selector </h1>
                <h4 style="text-align: center;color:black;font-weight: bolder;">Identify the best adapted varieties for your location</h4>
            </div>
        </div>
        <div class="row">
            <div class=" col-md-6">  

                <article style="background-image: linear-gradient(#dfff80, white);">
                    <h4>Smart variety choice for better harvest</h4>
                    <p>To get good yields, farmers need to grow a maize variety that
                        is well-adapted to its local environment. In the rain-fed conditions
                        under which most maize is grown, a variety will perform
                        well if the timing of sowing, flowering and grain maturity are
                        optimum in relation to rainfall and available soil moisture. This
                        means having enough water at the right time, and especially
                        avoiding water and heat stress at flowering, the most sensitive
                        stage of plant growth.</p>
                    <p><img src="images/planting_chart.png" alt="Planting"/></p>

                    <hr />

                    <ul type="disc">
                        <li>
                            If a variety flowers and matures too early (white bar), yield
                            will be below what can be achieved, because the growing
                            period of the variety was shorter than the length of the
                            growing season. All the available water was not exploited.

                        </li>
                        <li>
                            If a variety flowers and matures too late (yellow bar), yield
                            will be reduced due to lack of water and, in some cases, heat
                            stress. 
                        </li>
                    </ul>

                </article>


                <article class=" row">
                    <div class="col-md-8">
                        <h4>Maize-Variety-Selector app</h4>
                        <p>The mobile-phone application compares
                            times of flowering and maturity of different
                            maize varieties at the user’s location.*
                            The user enters his/her preferred planting
                            and harvesting dates, and the app displays
                            which varieties match those criteria. </p>
                        <p><span style="color:blue;font-weight: bolder">Maize-Variety-Selector</span> also has a
                            searchable database showing varietal
                            characteristics.</p>

                    </div>
                    <div class="col-md-4">

                        <p><img src="images/maize_variety_selector_app.png" alt="Planting"/></p> 
                    </div>
                    <ul type="disc">
                        <li>
                            * the location can be selected from a list, or determined
                            with the phone’s built-in GPS.
                        </li>
                    </ul>
                </article>


            </div>
            <div class=" col-md-6">  
                <article>
                    <h4>Predicting time to flowering and maturity</h4>
                    <p>Plant development and growth depends on temperature,
                        and in some cases on photoperiod (duration of daylight).
                        Flowering and maturity dates are routinely recorded in
                        breeding and variety testing trials, along with average daily
                        temperature from nearby weather stations. Our research
                        has developed equations that describe well the rate of
                        plant development against temperature. This means we can
                        now predict, for each maize variety and specific location,
                        the number of days it takes for the plant to flower and
                        reach maturity</p>
                    <p><img src="images/maturity_chart.png" alt="Planting"/></p> 
                </article>

                <article style="background-image: linear-gradient(white, #dfff80);">
                    <h4>Mapping varietal adaptation</h4>
                    <p>Flowering and maturity dates can be predicted for any
                        given sowing date, and at any given site, using spatial
                        temperature databases.</p>
                    <p>The predicted dates can be shown as maps that show
                        how duration varies, helping users compare varieties.</p>
                    <p><img src="images/maize_variety_map.png" alt="Planting"/></p> 

                </article>
            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->

<script src="<?php echo base_url() ?>includes/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="includes/vendors/select2/dist/js/select2.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function (e) {


    })


</script>

